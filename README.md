[![App Status](https://argo-cd-status-badge.grunlab.net/badge?name=sealed-secrets&revision=true&showAppName=true)](https://argo-cd.lan.grunlab.net/applications/sealed-secrets)

# GrunLab Sealed-secrets

Sealed-secrets deployment on Kubernetes.

Docs: https://docs.grunlab.net/install/sealed-secrets.md

GrunLab project(s) using this service:
- [grunlab/cups][cups]
- [grunlab/gitlab-runner][gitlab-runner]
- [grunlab/kubernetes-dashboard][kubernetes-dashboard]
- [grunlab/storj][storj]
- [grunlab/traefik][traefik]
- [grunlab/vigixplorer][vigixplorer]
- [grunlab/zabbix][zabbix]
- [grunlab/zoneminder][zoneminder]


[cups]: <https://gitlab.com/grunlab/cups>
[gitlab-runner]: <https://gitlab.com/grunlab/gitlab-runner>
[kubernetes-dashboard]: <https://gitlab.com/grunlab/kubernetes-dashboard>
[storj]: <https://gitlab.com/grunlab/storj>
[traefik]: <https://gitlab.com/grunlab/traefik>
[vigixplorer]: <https://gitlab.com/grunlab/vigixplorer>
[zabbix]: <https://gitlab.com/grunlab/zabbix>
[zoneminder]: <https://gitlab.com/grunlab/zoneminder>